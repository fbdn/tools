#!/usr/bin/env python
# -.- encoding: utf-8 -.-

from simplecrypt import encrypt, decrypt
from random import randint, choice
import os, linecache
import hashlib, string


def rand_string(n):
    '''
        Generates a random uppercase string. Nums and A-Z
        Stolen at stackexchange.
    '''
    return ''.join(
                choice(
                    string.ascii_uppercase +
                    string.digits
                    )
                for _ in range(n)
                )


def RandomWord():
    ''' Generates a random word from the linux dictionary '''
    dictpath = '/usr/share/dict/words' # Path to dictionary text file
    with open(dictpath) as dictfile: # Open file only for following operation
        number_of_lines = sum(1 for lines in dictfile) # Count number of words
    randline = randint(0,number_of_lines) # Define area for word choosing
    line = linecache.getline(dictpath,randline) # Getline from linecache lib
    # Remove plural's and newline
    line = line.replace("'s", '').replace('\n', '');
    print line # ...
    return line # returns the randomly chosen line. Tadaa :)


class User: # Define raw object user
    def __init__(self, **kwargs): # initialize/create real object of that kind
        '''
            Main User Object
            Has:
            * Any keyword you'd like it to have
                (kwargs can be like: u = User(
                                            user_is_hungry = True, fischfilet,
                                            ["we", "can", "haz", "list", True],
                                            {dict: "should work too"}))
            * An established session to one other user
            * Messages
            * A public key
            * A secret key
         '''

        # Initialize empty vars, to prevent nonvalue errors
        self.args = kwargs
        self.pub_key = None
        self.session = None
        self.messages = {}
        self.password = ""
        self.role = "user"
        print kwargs
        # self.name = raw_input("Enter username: \n> ")

        try: # Try to read user details from arguments
            # omg kwargs are so handy! Just give it to functions
            # and pass them anything you like!
            # Access them later as dictionaries:
            self.name = kwargs["name"]
            self.secret_key = kwargs["secret"]
            self.role = kwargs["role"]
            self.password = kwargs["password"]
            print self.name, self.secret_key, self.role

        except(KeyError): # If there is no such item in the dict
            self.name = "default" # Generate a random one
            self.secret_key = 23 # Also for the secret
            # self.secret_key = randint(5,19) # Also for the secret
            self.password = "1234"
            # print self.__init__.__doc__ # Print the docs (just in case...)

    def generate_key(self):
        '''
            # First part of diffie-hellman math stuff
            => en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange

            1. Alice and Bob agree to use a modulus p = 23 and
                base g = 5 (which is a primitive root modulo 23).
            => See register_session

            2. Alice chooses a secret int a = 6,then sends Bob A = g^a mod p
            * A = 5^6 mod 23 = 8
            * A = 5**6 % 23 = 8

            3. Bob chooses a secret int b = 15, then sends Alice B = g^b mod p
            * B = 5^15 mod 23 = 19

            Further see check_secret

        '''
        self.pub_key = self.session.base ** self.secret_key % self.session.mod
        print "[%s] PubKey: %s" % (self.name, self.pub_key)

    def register_session(self, session):
        '''
            Session registration, called externally.
            Requries session object and appends it to the current session.
            After that it triggers the generation of the public key
        '''
        self.session = session
        self.generate_key()
        print "[%s] Registered %s [%d/%d]:" % (
                                                self.name,
                                                self.session.name,
                                                self.session.base,
                                                self.session.mod)

    def check_secret(self, partner_key):
        '''
            This does the other end of the exchange
            Gets "public key" of other participant

            4. Alice computes s = B^a mod p
                s = 19^6 mod 23 = 2

            5. Bob computes s = A^b mod p
                s = 8^15 mod 23 = 2
                s = 8**15 % 23 = 1
            6. Alice and Bob now share a secret (the number 2).

            Yay.
        '''
        self.shared_secret = partner_key ** self.secret_key % self.session.mod
        print "[%s] Shared secret: %d" % (self.name, self.shared_secret)

    def send_message(self, reciever, text):
        '''
            Creates a new message object with given details
            Implemented simplecrypt lib to encrypt text

            Args:
            * Reciever object
            * plaintext message
        '''
        print "[%s] Send Message to %s: %s" %(self.name, reciever, text)
        crypto_msg = encrypt(str(self.shared_secret), text)
        msg = Message(self, reciever, crypto_msg)

    def check_messages(self):
        ''' Simply iterates over user.messages dictionary and decrypts '''
        for key, msg in self.messages.iteritems():
            try:
                plain_text = decrypt(str(self.shared_secret), msg.text)
            except:
                plain_text = "Sry, undecryptable :("
            print "\n\n[%s] Inbox\n==================\n" % self
            print "#", msg.name
            print "%s: %s" % (msg.sender, plain_text)

    def __str__(self):
        '''
            Meta class. When object User is called, the given
            string is printed insteal of object descriptor.
        '''
        return self.name


    def update_pw(self, new_pw):
        self.password = ""
        self.password = self.password.join(new_pw)
        return True


class Message:
    count = 0
    current = None
    def __init__(self, sender, reciever, text):
        Message.count += 1
        self.name = rand_string(4)
        self.text = text
        self.sender = sender
        self.reciever = reciever
        Message.current = self.name
        print "[%s] Message" % (self.name)
        reciever.messages[self.name] = self
        def __str__(self):
            return self.name


class Session:
    count = 0
    current = None
    def __init__(self):
        self.name = rand_string(4)
        self.mod = randint(99,999)
        self.base = randint(30,99)
        Session.current = self.name
        Session.count += 1
        print "[%s] New Session" % self.name
        print "[%s] Modulus: %d" % (self.name, self.mod)
        print "[%s] Base: %d" % (self.name, self.base)



# def main():
    # alice = User(name="alice", secret=6)
    # bob = User(name="bob", secret=15)

    # asd = Session()

    # alice.register_session(asd)
    # bob.register_session(asd)

    # alice.check_secret(bob.pub_key)
    # bob.check_secret(alice.pub_key)

    # alice.send_message(bob, "hi, whut up")
    # bob.check_messages()

    # bob.send_message(alice, "Hey, just shizzling around :D")
    # alice.check_messages()

# main()

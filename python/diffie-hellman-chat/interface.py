#!/usr/bin/env python
# -.- encoding: utf-8 -.-


import getpass, os, sys, hashlib, pickle
from tabulate import tabulate
from dhe import User, Session, Message


class Interface(object):
    user_list = []
    user_pos = None
    def __init__(self):
        self.get_user_data()

    def clean_exit(self):
        user_db = open('user_db.txt', 'wb')
        pickle.dump(len(self.user_list), user_db)
        for usr in self.user_list:
            pickle.dump(usr, user_db)
        sys.exit()

    def get_user_data(self):
        # Datei nicht da? -> erstellen
        if os.path.exists("user_db.txt") == False:
            user_db = open("user_db.txt", "w")
        else:
            filehandler = open("user_db.txt", "rb")
            try:
                for _ in range(pickle.load(filehandler)):
                    self.user_list.append(pickle.load(filehandler))
            except:
                print "[Error] DB file unreadable"


    def login(self):
        try:
            loginname = str(raw_input("login: "))
        except KeyboardInterrupt:
            self.clean_exit()
            print "Bye"
            self.clean_exit()
        try:
            loginpw = getpass.getpass("password: ")
        except:
            self.clean_exit()

        # TODO: Benutzer 1 & 1 entfernen :) (bei Abschluss von Entwicklungsarbeiten)
        if loginname == "1" and loginpw == "1":
            self.user_pos = 0
            try:
                self.user_list[self.user_pos] = User(
                                    name="1",
                                    secret=666,
                                    role="admin"
                                    )
                print "[OK] Created admin at 0"
            except:
                self.user_list.append(User(
                                    name="1",
                                    secret=666,
                                    role="admin")
                                    )
                print "[OK] Append admin"
            return True
        for position, user in enumerate(self.user_list):
            if user.name == loginname and user.password == hashlib.md5(loginpw).hexdigest():
                self.user_pos = position
                print "login ok!"
                return True
        return False


    def start(self):
        not_restart = True
        while not_restart == True:
            # Benutzereingabe:
            try:
                command_full = str(raw_input("> "))
            except:
                self.clean_exit()
            command_full_list = command_full.split(' ')
            command_parameter = []
            # Zähle von 1 bis zur Länge der Liste der Befehle  (=command_full_list)
            for i in range (1, len(command_full_list)):
                command_parameter.append(command_full_list[i])

            # Interpretiere den Befehl
            not_restart = self.cli_interpreter(command_full_list[0], command_full_list)
        print "\n \t --- Done! ---\n"
        print "\n"


    def cli_interpreter(self, command, command_list):
            # Wenn die Eingabe in folgender Liste ist:
            if command in [ "list", "l" ]:
                if self.user_list[self.user_pos].role != "admin":
                    self.show_no_permission()
                    return True
                self.show_list(command, command_list)
            elif command in [ "insert" , "i", "append", "a" ] :
                if self.user_list[self.user_pos].role != "admin":
                    self.show_no_permission()
                    return True
                self.insert_user()
            elif command in ["show", "s", "sh"]:
                self.show_person()
            elif command in ["message", "m", "msg"]:
                self.check_message()
            elif command in ["send", "se"]:
                self.send_message_to()
            elif command in [ "exit" , "x", "quit", "q" ] :
                print "Progamm beendet"
                self.clean_exit();
            elif command in [ "help", "h" ]:
                self.show_help()
            elif command in [ "logout", "d", "write", "w"]:
                return False
            elif command in ["passwort", "passwd", "p", "pw"]:
                self.change_pw()
            elif command == "":
                pass
            else:
                print "[!] Command not recognized. h or help for Help Page"
            return True


    def show_no_permission(self):
        print "[!] Access denied!\n"

    def show_list(self, command, command_list):
        list_to_show = []
        for pos, person in enumerate(self.user_list): # Zählt die Objekte der user_list durch
            list_to_show.append([pos, person.name, person.role, person.secret_key]) #
        print tabulate(list_to_show, headers=["#", "name", "role", "Key"],
              tablefmt='rst')

    def insert_user(self):
        while True:
            new_user_name = str(raw_input("Name des User:\n  "))
            for i, person in enumerate(self.user_list):
                if new_user_name == person.name:
                    print "Name vergeben, bitte neuen wählen:\n>"
                    continue
            break

        while True:
            try:
                new_user_secret = int(raw_input("Secret:\n  "))
                break
            except ValueError:
                    print("Bitte eine korrekte Nummer angeben")
        new_user_role = ""
        while new_user_role not in [ "admin", "user" ]:
            new_user_role = str(raw_input("role (admin/user):\n"))
        newpass = "0"
        newpassr = "1"
        while newpass != newpassr:
            try:
                newpass = getpass.getpass("Passwort: ")
                newpassr = getpass.getpass("Wiederholung: ")
            except:
                print "Das ist wohl schief gegangen"
        new_user_pwhash = str(hashlib.md5(newpass).hexdigest())
        new_user = User(name=new_user_name, password=new_user_pwhash, role=new_user_role, secret=5)
        self.user_list.append(new_user)
        print "User hinzugefügt"

    def show_person(self):
        print "UserIn: ", self.user_list[self.user_pos].name

    def show_help(self):
        list_to_show = [[ "list, l" ,  "List persons <op only>"],
                [ "insert , i, append, a" , "Add User <op only>" ],
                ["show, s, sh", "Show personal info" ],
                ["send, se", "Send Message" ],
                [ "exit , x, quit, q" , "Quit" ],
                [ "help, h" , "Show Help" ],
                [ "m" , "msg" "Geld Abheben" ],
                [ "logout, d, write, w", "Beendet die Sitzung" ],
                ["passwort, passwd, p, pw", "Aendert das Passwort" ]]

        print "Mögliche Befehle:"
        print tabulate(list_to_show, headers=["Befehl", "Bedeutung"], tablefmt='orgtbl')

    def check_message(self):
        self.user_list[self.user_pos].check_messages()

    def send_message_to(self):
        reciever = str(raw_input("Empfänger: "))
        for i, person in enumerate(self.user_list):
                if reciever == person.name:
                    message = str(raw_input("Message:\n> "))

                    ses = Session()

                    person.register_session(ses)
                    self.user_list[self.user_pos].register_session(ses)

                    person.check_secret(self.user_list[self.user_pos].pub_key)
                    self.user_list[self.user_pos].check_secret(person.pub_key)


                    self.user_list[self.user_pos].send_message(reciever=person, text=message)
                    print "Erfolgreich verschickt"
                    return
        print "Empfänger nicht gefunden."



    def change_pw(self):
        newpass = "0"
        newpassr = "1"
        while newpass != newpassr:
            try:
                newpass = getpass.getpass("Bitte neues Passwort angeben:\n>")
                newpassr = getpass.getpass("Wiederholung:\n>")
            except:
                print "Das ist wohl schief gegangen"
        new_pw = str(hashlib.md5(newpass).hexdigest())
        if self.user_list[self.user_pos].update_pw(new_pw) == True:
            print "Okidoki, Passwort geändert"



###############################################################################
#######                          START CODE                             #######
###############################################################################

user_session = Interface()
while True:
    print "\n \t --- Mail Server ---\n"
    if user_session.login() == True:
        user_session.start()
        print "\n"

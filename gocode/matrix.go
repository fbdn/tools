package main

import (
    "github.com/stianeikeland/go-rpio"
    "time"
    "log"
    "strconv"
)

func sleep(d string) {
    dur, err := time.ParseDuration(d)
    if err != nil {log.Fatal(err)}
    time.Sleep(dur)
}

type LED struct {
    name string
    pinX rpio.Pin
    pinY rpio.Pin
}

func (led *LED) On(t string) {
    go func(){
      duration, err := time.ParseDuration(t)
      if err != nil {log.Fatal(err)}
      led.pinX.High()
      time.Sleep(duration)
      led.pinX.Low()
    }()
}

func NewLED(name string, pinX int, pinY int) LED {
    var l = LED{name: name, pinX: rpio.Pin(pinX), pinY: rpio.Pin(pinY)}
    l.On("500ms")
    return l
}

type Matrix struct {
    dots map[string]LED
    name string
}


func NewMatrix(name string, pinsX []int, pinsY []int) Matrix {
    if err := rpio.Open(); err != nil {
        log.Fatal(err)
    }
    defer rpio.Close()

    var m = Matrix{name: name}

    var ledlen = len(pinsY)+len(pinsX)
    leds := make([]LED, ledlen)
    log.Println(ledlen)
    for x := range pinsX {
        for y := range pinsY {
            dotname := "x" + strconv.Itoa(x) + "y" + strconv.Itoa(y)
            leds[x] = NewLED(dotname, pinsX[x], pinsY[y])
            log.Println(m)
            m.dots[dotname] = NewLED(dotname, pinsX[x], pinsY[y])

            log.Printf(dotname)
        }
    }
    return m
}


func main() {
    NewMatrix("AAAh", []int{26,6}, []int{10,20})

    /*  Example Usage
    l2 := NewLED("x1y1", 10,22)
    sleep("2s")
    log.Println(l2)
    */
}


/*
The go-rpio library use the raw BCM2835 pinouts, not the ports as they are mapped
on the output pins for the raspberry pi
   Rev 1 Raspberry Pi
   +------+------+--------+
   | GPIO | Phys | Name   |
   +------+------+--------+
   |   0  |   3  | SDA    |
   |   1  |   5  | SCL    |
   |   4  |   7  | GPIO 7 |
   |   7  |  26  | CE1    |
   |   8  |  24  | CE0    |
   |   9  |  21  | MISO   |
   |  10  |  19  | MOSI   |
   |  11  |  23  | SCLK   |
   |  14  |   8  | TxD    |
   |  15  |  10  | RxD    |
   |  17  |  11  | GPIO 0 |
   |  18  |  12  | GPIO 1 |
   |  21  |  13  | GPIO 2 |
   |  22  |  15  | GPIO 3 |
   |  23  |  16  | GPIO 4 |
   |  24  |  18  | GPIO 5 |
   |  25  |  22  | GPIO 6 |
   +------+------+--------+
   See the spec for full details of the BCM2835 controller:
   http://www.raspberrypi.org/wp-content/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
*/

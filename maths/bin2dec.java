import java.util.Scanner; // Import extra library for user input
 // Public makes class visual and callable for other classes/methods
public class bin2dec {
    public static void MathStuff() { // Also methods
        System.out.println(".............\n");
        System.out.println("Enter Number:");
        Scanner scan = new Scanner(System.in); // Create Scanner object
        String UserInput = scan.next(); // Query a scanner input
        System.out.println("Enter Base:");
        int b = scan.nextInt();  // The base, that is to be multiplied with
        int l = UserInput.length(); // Length of the string
        int result = 0;
        int n = (l-1); // The index from 0, which is to be power to be taken to
        System.out.println("String length: "+l); // for the user experience!
        for (int i = 0; i <= n; i++) { 
            // As long as i is smaller or equal to our index: count i plus 1
            char LocalChar = UserInput.charAt(i); // Get character on index pos
            int z = Character.getNumericValue(LocalChar); // Get value as Number
            // Now multiply the local decimal value with our base, that is taken
            // to the power of the local index minus i, because we start at n-1
            int v = z*(int)Math.pow(b, n-i);
            result=result+v; // Add it up to our sum
            System.out.println(z+"*"+(b)+"^"+(n-i)+" = "+v); // And for the ux!
        }
        System.out.println("Final result: "+result);
        MathStuff(); // What a fun! Lets do the method right again --> 
    }
public static void main(String[] args) {
    // Javac compiler always looks for a main method. Guess related to object
    // construcion and/or multi-file-handling
    MathStuff(); 
    }
}

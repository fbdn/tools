import java.util.Scanner; // Import extra library for user input

// TODO: Include floating point values, dec2bin, ascii2bin [...]

 // Public makes class visual and callable for other classes/methods
public class NumberSystemCalc {
    String ConvertTo;
    String UserInput;
    int Base;
    int StringLength;
    boolean isFloat;


    public static void print(Object toPrint){System.out.println(toPrint);}


    private void GetInput() { // Also valid for methods
        Scanner scan = new Scanner(System.in); // Create Scanner object
        print(".............\n");
        print("Convert to\n[1]*Decimal\n[2] Other\n[3] Binary");
        this.ConvertTo = scan.next(); // Query a scanner input
        print("Enter Number:"); this.UserInput = scan.next(); // Query a input
        print("Enter Base:"); this.Base = scan.nextInt();  // The base
        this.StringLength = this.UserInput.length(); // Length of the string
    }


    public void ToDecimal() {
        // The index from 0, which is to be power to be taken to        
        int n = (this.StringLength-1);
        if (UserInput.indexOf(",") >= 0) {
            n = n; 
            int positiveValues = UserInput.indexOf(",");
            int negativeValues = StringLength-UserInput.indexOf(",");
            print("String is float: "+positiveValues+","+negativeValues);
        }

        float result = 0f;
        boolean AfterComma=false;
        int j = -1;
        print("Number length: "+n);

        for (int i = 0; i <= n; i++) { 
            // As long as i is smaller or equal to our index: count i plus 1
            char LocalChar = this.UserInput.charAt(i); // Get char on index pos
            char separator = ',';
            if (LocalChar == separator) {
                AfterComma = true;
                print("Now after comma");
                continue; }
            
            int z = Character.getNumericValue(LocalChar); // Get value as number
            print(z);
            if (AfterComma == true) {
                print(j);
                float v = z*(float)Math.pow(Base, j);
                result=result+v; // Add it up to our sum
                print(z+"*"+(Base)+"^"+(j)+" = "+v); // For the ux!
                j = j-1;
            } else {
            // Now multiply the local decimal value with our base, that is taken
            // to the power of the local index minus i, because we start at n-1
            int v = z*(int)Math.pow(Base, n-i);
            result=result+v; // Add it up to our sum
            print(z+"*"+(Base)+"^"+(n-i)+" = "+v); // For the ux!
            }
        }
        
        print("Final result: "+result);
        // Suddenly error here. Worked that way yesterday -.-
        DoDaStuff(); // What a fun! Lets do the method right again --> 
    }

    public static void DoDaStuff() {
        NumberSystemCalc nCalc = new NumberSystemCalc();
        nCalc.GetInput();
        if (nCalc.ConvertTo == "2") {
            System.out.println(nCalc.ConvertTo);
        } else {
            nCalc.ToDecimal();
        }
    }

    public static void main(String[] args) {
        // Javac compiler looks for a main method. Guess related to object
        // construcion and/or multi-file-handling
        System.out.println("Welcome");
        DoDaStuff();
    }
}

#!/usr/bin/python
# -*- coding: utf-8 -*-

input_string = raw_input("Plz enter binary number:\n> ")
length = len(input_string)
ergebnis = 0 
for i in reversed(xrange(0, length)):
  print input_string[(length-1)-i],"*2^",i,"=", \
  (int(input_string[(length-1)-i])*2**i)
  ergebnis = ergebnis + int(input_string[(length-1)-i])*2**i
print "Ergebnis: ", ergebnis


# or like this one:
# b = raw_input("Plz enter binary number:\n> "); r = 0
# for i in reversed(xrange(0,len(b))):r=r+int(b[(len(b)-1)-i])*2**i
# print "Result: ",r

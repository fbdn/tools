import javax.swing.JOptionPane;
import java.util.ArrayList;

public class EulerCalculator {
    public static void sumof3and5multiples() {
        int base3 = 3;
        int base5 = 5;
        int sumof = 0;
        int limit = 1000;
        for (int counter = 1; counter < limit; counter++) {
            if ((counter % base5 == 0) && (counter % base3 == 0)) {
                sumof = sumof+counter;
                System.out.println("Reached fifteen");
                continue;
            }
            if (counter % base5 == 0) {
                sumof = sumof+counter;
                System.out.println("Counter = "+counter);
                System.out.println("Sum = "+sumof);
            }
            if (counter % base3 == 0) {
                sumof = sumof+counter;
                System.out.println("Counter = "+counter);
                System.out.println("Sum = "+sumof);
            }
        }
    }

    public static void evenfibonacci() {
        int limit = 4000000;
        int a = 1;
        int b = 2;
        int c = 0;
        int sumof = 0;
        int counter = 0;
        while (b < limit) {
            c = a + b;
            a = b;
            b = c;
            counter++;
            System.out.println("Fibonacci"+counter+" = "+c);
            if (a % 2 == 0) {
                sumof = sumof + a;
                System.out.println("Even!"+ a + " = "+sumof);
            }
        }
    }

    public static void primefactors() {
        //~ long number = 600851475143L;
        long number = 6008514751L;
        long result = 0L;
        long prime = 0L;
        long counter = 2L;
        ArrayList<Long> primes = new ArrayList<Long>();

        for (;counter < number; counter++) {
            iterations: {
                for (long tester = 2L; tester < counter; tester++) {
                    if (counter % tester == 0){
                        //~ not_primes.add(counter);
                        break iterations;}}

                primes.add(counter);
            }
        }
    System.out.println("Primes: "+primes);
    System.out.println("----------------");
    System.out.println("Calculating prime factors of "+number);



    }

    public static void Calculator() {
        String NumberOne = JOptionPane.showInputDialog("Zahl 1:");
        String NumberTwo = JOptionPane.showInputDialog("Zahl 2:");
        String[] operations = { "+", "-", "/", "*" };
        String OperationChoice = (String) JOptionPane.showInputDialog(null, "Choose Operation", "Best... Calculator... Ever!", JOptionPane.QUESTION_MESSAGE, null, operations, operations[0]);
        System.out.println(OperationChoice);
        try {
            int NumberOneAsInt = Integer.valueOf(NumberOne);
            int NumberTwoAsInt = Integer.valueOf(NumberTwo);
        } catch (Exception KackFehler) {
            JOptionPane.showMessageDialog(null, "Keine Zahl", "Fehler!", JOptionPane.ERROR_MESSAGE);
        }
    }


    public static void main(String[] args) {
        //~ sumof3and5multiples();
        //~ evenfibonacci();
        primefactors(); // Baustelle!
        //~ Calculator();
    }
}

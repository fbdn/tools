import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
//  import java.util.AbstractList;


public class stuff {
    static Scanner my_scanner = new Scanner(System.in);


    public static double distanceOrigin(double x, double y){
        double c = Math.pow(x, 2) + Math.pow(y, 2);
        c = Math.sqrt(c);
        System.out.println(c);
        return c;
    }

    public static double cubeVolume(double x){
        System.out.println(Math.pow(x, 3));
        return Math.pow(x, 3);
    }


    public static double cubeSurface(double x){
        System.out.println(Math.pow(x, 2) * 6);
        return Math.pow(x, 2) * 6;
    }

    public static String stringInsert(String str, int pos){

        stringInsert("HIPAT", 2);

        List<Character> chars = new ArrayList<Character>();
        
        for (char c : str.toCharArray())
        {
            chars.add(c);
        }

        String newstring = "";
        chars.add(pos, "_".charAt(0));
        System.out.println(chars);
        for(char c : chars){
            newstring = newstring + c;
        }
        System.out.println(newstring);
        return newstring;
    
    }

    public static void weird_numbers(){
        int number = my_scanner.nextInt();
        if (number % 2 != 0) {
            System.out.println("WEIRD!");
        } else if (number >= 2 && number <= 5) {
            System.out.println("NOT WEIRD!");
        } else if (6 <= number && number <= 20){
            System.out.println("WEIRD!");
        } else if (number > 20) {
            System.out.println("NOT WEIRD!");
        } else if (number < 0) {
            System.out.println("WEIRD!");
        }
    }
    
    
    public static void addition(){
        int number1 = my_scanner.nextInt();
        int number2 = my_scanner.nextInt();
        System.out.println(number1 + number2);
    }

    public static void whats_your_name(){
        String eingabe = my_scanner.nextLine();
        String vorname = eingabe.split(" ")[0];
        String nachname = eingabe.split(" ")[1];
        System.out.println("Hallo " + vorname);
        System.out.println("Voller name: "+ vorname + " " + nachname);
    }
    

    public static void stdin_num() {
        int[] numbers = new int[4];

        System.out.println("Iterieren mit For-Schleife");
        for (int i = 0; i <= numbers.length; i++)
        {
            numbers[i] = my_scanner.nextInt();
            System.out.println(numbers[i]);
        }

        System.out.println("Iterieren mit While-Schleife");
        int counter = 0;
        while (counter < 3){
            numbers[counter] = my_scanner.nextInt();
            System.out.println(numbers[counter]);
            counter++;
            
        }
    }


    public static void main (String args[]) {
        //  stdin_num();
        //  whats_your_name();
        //  addition();
        //  weird_numbers();
        //  clock_tanslator();
        //  distanceOrigin(4, 3);
        //  cubeVolume(4);
        //  cubeSurface(2);
        stringInsert("HIPAT", 2);
    }
    
}
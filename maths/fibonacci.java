public class fibonacci {
   public static long maciavelli(long number) {
      if ((number == 0) || (number == 1))
         return number;
      else
         return maciavelli(number - 1) + maciavelli(number - 2);
   }
   public static void main(String[] args) {
      for (int counter = 0; counter <= 10; counter++){
         System.out.printf("Fibonacci of %d is: %d\n",
         counter, maciavelli(counter));
      }
   }
}

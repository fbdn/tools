#!/usr/bin/env python

import argparse, gdapi, sys


""" Usage
object Host "rancher-1" {

  [...]

  vars.rancher = "true"
  vars.rancher_url = "http://x.x.x.x:8080/v2-beta" # link to api
  vars.rancher_access_key = "KEY"  # Authentication token
  vars.rancher_secret_key = "SECRET"  # Corresponding token
  vars.rancher_host = "rancher-1"  # This node's name as shown in Hosts list
  vars.rancher_env = "Default" # Environment name
  vars.rancher_endpoints = "x.x.x.1,x.x.x.2"  # IP's to look for the endpoint at
  vars.rancher_endpoint_ports = "80,443"  # Ports to check


  vars.rancher_stacks["NRPE"] = {
    rancher_env = "Default"
    rancher_stack = "nrpe"}
}
"""
 

def icinga_exit(x, msg):
  """ Converts exit number to string and attaches message """
  statuses = {0:"OK", 1:"WARNING", 2:"CRITICAL", 3:"UNKNOWN"}
  try:
    exit_code=int(x)
    exit_code = x if 0 <= x <= 3 else 3
  except Exception as e:
    exit_code=3
    msg = e
  status = statuses[int(exit_code)]
  print "%s - %s" % (status, msg)
  sys.exit(exit_code)



def connect_client(api, key, secret):
  """ Try to connect to the api """
  try:
      return gdapi.Client(url=api, access_key=key, secret_key=secret)
  except Exception as e:
      icinga_exit(3, e)



def get_stack_health(stack_name, client, env_id):
  """ Query all stacks and filter for desired stack """
  try:
    # Stupid nesting and unpacking of data objects
    stacks = client.by_id_project(env_id).stacks().data
  except Exception as e:
    icinga_exit(3, e)

  # Find desired stack
  try: 
    current_stack = [s for s in stacks if s["name"] == stack_name][0]
  except: 
    icinga_exit(3, "Stack not found:" + stack_name)

  health = current_stack["healthState"]
  state = current_stack["state"]
  msg = "Stack {} is {} and {}".format(stack_name, state, health)

  if health == "healthy" and state == "active":
    icinga_exit(0, msg)
  if health in ["unhealthy", "degraded"]:
    icinga_exit(2, msg)
  if state == "inactive":
    icinga_exit(1, msg)
  else:
    icinga_exit(3, "Unknown Stack Problem: "+msg)



def get_host_health(host, client, env_id):
  """ Query hosts and filter for our host """
  try:
    hosts = client.by_id_project(env_id).hosts()
  except Exception as e:
    icinga_exit(3, e)
  
  for h in hosts:
    name = h["name"]
    hostname = h["hostname"]
    if host in [name, hostname]:
      ip = h["agentIpAddress"]
      state = h["state"]
      msg = "Host {} ({}/{}) is {}".format(host, hostname, ip, state)
      if state == "active":
        icinga_exit(0, msg)
      else:
        icinga_exit(2, msg)
    icinga_exit(3, host+" not found.")



def get_service_health(service_name, stack_name, client, env_id):
  """ Query stacks, filter for our stack and get the service """
  stacks = client.by_id_project(env_id).stacks().data
  try: current_stack = [s for s in stacks if s["name"] == stack_name][0]
  except: icinga_exit(3, "Stack not found")
  services = current_stack["services"]()

  for service in services:
    if service["name"] == service_name:
      state = service["state"]
      health = service["healthState"]
      msg = "Service {} is {} and {}".format(service_name, state, health)

      if state == "upgrading" and health == "degraded":
        icinga_exit(1, msg)
      if state == "restarting" and health == "healthy":
        icinga_exit(1, msg)
      if health == "healthy":
        icinga_exit(0, msg)
      else:
        icinga_exit(2, msg)
      break

  if stack is None: 
    icinga_exit(3, "Service checks need a Stack")
  icinga_exit(3, "Oops, you broke it...")



def check_ip(client, env_id, endpoints, endpoint_ports):
  try:
    hosts = client.by_id_project(env_id).hosts()
  except Exception as e:
    icinga_exit(3, e)
  
  endpoints_list = [ p for p in endpoints.split(",") ]
  endpoint_ports_list = [ int(p) for p in endpoint_ports.split(",") ]
	
  endpoint_errors = []
  for h in hosts:
    hostname = h["name"]
    endpoints = h["publicEndpoints"]
    for e in endpoints:
        ip = e["ipAddress"]
        port = e["port"]
        for ep in endpoints_list:
          if ip == ep: 
            if port in endpoint_ports_list:
		msg = "Public Endpoint on {} has IP-Adress {} and Port {}".format(hostname, ip, port)
                endpoint_errors.append(msg)
  if endpoint_errors != []:
    msg = "".join(x+"\n" for x in endpoint_errors)
    icinga_exit(2, msg)
  msg = "All endpoints are fine."
  icinga_exit(0, msg)


def parseargs():
  p = argparse.ArgumentParser(description='Icinga2 checks for Rancher')
  p.add_argument('-api', type=str, help='Rancher api link')
  p.add_argument('-key', type=str, help='Access key')
  p.add_argument('-secret', type=str, help='Secret key')
  p.add_argument('-env', type=str, help='Environment', default="Default")
  p.add_argument('-host', type=str, help='target host')
  p.add_argument('-stack', type=str, help='target Stack')
  p.add_argument('-service', type=str, help='Service (requires a stack)')
  p.add_argument('-endpoints', type=str, help='Public endpoint ip to check')
  p.add_argument('-endpoint_ports', type=str, help='Public endpoint ports to check')
  return p.parse_args()



def main():
  args = parseargs()
  client = connect_client(args.api, args.key, args.secret)
  projects = client.list_project()
  try: 
    # To fetch the environment
    env_id = [e for e in projects if e["name"] == args.env][0]["id"]
  except: 
    icinga_exit(3, "Environment not found: " + args.env)

  # Vars are None if not passed by user

  if args.endpoints != None and args.endpoint_ports != None:
    check_ip(client, env_id, args.endpoints, args.endpoint_ports)

  if args.host is None:
    if args.service is None:
      get_stack_health(args.stack, client, env_id)
    get_service_health(args.service, args.stack, client, env_id)
  get_host_health(args.host, client, env_id)


if __name__ == "__main__":
  main()


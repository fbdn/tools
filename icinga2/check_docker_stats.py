import os
import sys
import re


try:
    docker_ps = os.popen("docker ps -q").read()
except:
    print("Something went wrong!")
    sys.exit(1)

for container in docker_ps.split("\n")[:-1]:
    # docker_stats_string = 'docker stats --no-stream --format "table {{{{.Name}}}}...{{{{.CPUPerc}}}}...{{{{.MemUsage}}}}" {{}}|tail -n +2'.format(container)
    docker_stats_string = 'docker stats --no-stream {}|tail -n +2'.format(container)
    print(docker_stats_string)
    docker_stats = os.popen(docker_stats_string)
    docker_stats = docker_stats.read()
    docker_stats = re.split('\s{2,}', docker_stats)
    print(docker_stats)
    container_name = docker_stats[0]
    container_cpu = docker_stats[1]
    container_mem_byte = docker_stats[2]
    container_mem_percent = docker_stats[3]
    container_netIO = docker_stats[4]
    container_blockIO = docker_stats[5]

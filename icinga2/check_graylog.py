#!/usr/bin/env python

import argparse, sys, requests
from check_rancher import icinga_exit


""" Usage
object Host "graylog" {                                                     
  import "generic-host"                                                     
                                                                            
  display_name = "Graylog"                                                  
                                                                            
  address = "x.x.x.x"                                                       
                                                                            
                                                                            
vars.gl_health = "https://graylog:9000/api/system"                          
vars.gl_notifications = "https://graylog:9000/api/system/notifications"     
vars.gl_user = "me"                                                         
vars.gl_pw = "you"                                                          

"""

requests.packages.urllib3.disable_warnings(
    requests.packages.urllib3.exceptions.InsecureRequestWarning
    )


arguments = {
    "Description": "Icinga2 Checks against the Graylog API",
    "link": "Graylog API link",
    "user": "Graylog API User",
    "pw": "Graylog API Password",
    }

def parser_factory(descr, arguments):
  p = argparse.ArgumentParser(description=arguments["Description"])
  for k,v in arguments.items():
    if k != "Description":
      p.add_argument('-'+str(k), type=str, help=str(v))
  return p




def parseargs():
  p = argparse.ArgumentParser(description='Icinga2 checks for Graylog')
  p.add_argument('-l', type=str, help='Graylog api link')
  p.add_argument('-u', type=str, help='User name')
  p.add_argument('-p', type=str, help='User password')
  return p.parse_args()


def notifications_status(response):
  check_passed = []
  notification_count = response["total"]
  notification_msg = "{} Notifications".format(notification_count)
  check_values = [notification_msg]
  if notification_count < 1:
    icinga_exit(0, ", ".join(x for x in check_values))

  for n in response["notifications"]:
    check_values.append("{} ({})".format(n["type"], n["severity"]))
    okay = True if n["severity"] == "normal" else False
    check_passed.append(okay)
  if False in check_passed:
      icinga_exit(2, ", ".join(x for x in check_values))
  icinga_exit(1, ", ".join(x for x in check_values))


def system_status(response):
    checks = ["lb_status", "is_processing", "lifecycle"]
    whitelist = ["running", "True", "alive"]
    check_values = []
    check_passed = []

    for check in checks:
        value = str(response[check])
        check_values.append("{} is {}".format(check, value))
        okay = True if value in whitelist else False
        check_passed.append(okay)

    if False in check_passed:
        icinga_exit(2, ", ".join(x for x in check_values))
    else:
        icinga_exit(0, ", ".join(x for x in check_values))


def get_response(link, user, password):
  try:
    auth = requests.auth.HTTPBasicAuth(user, password)
    response = requests.get(link, verify=False, auth=auth).json()
  except Exception as e:
    icinga_exit(3, e)
  return response


def main():
  try:
    args = parseargs()
    response = get_response(args.l, args.u, args.p)
  except Exception as e:
    icinga_exit(3, e)

  if args.l.endswith("notifications"):
    notifications_status(response)
  if args.l.endswith("system"):
    system_status(response)

if __name__ == "__main__":
  main()

